export const initialState = {
    slider1 : 70,
    slider2 : 10
}
  
export const reducer = (state, action) => {
    switch (action.type) {
        case 'UPDATE_SLIDER' : 
            return {
                ...state,
                [action.namespace]: action.payload
            }
        default :
            return state
    }
}