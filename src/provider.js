
import React, { createContext, useReducer } from 'react';
import {reducer, initialState } from './reducer';

export const AppContext = createContext();

export const AppProvider = ({ children }) => {
  const contextValue = useReducer(reducer, initialState);
  return (
    <AppContext.Provider value={contextValue}>
      {children}
    </AppContext.Provider>
  );
};