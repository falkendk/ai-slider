import React from 'react';
import { AppProvider } from './provider';

import Slider from './components/slider/Slider';
import './App.scss';

function App() {
  return (
      <div className="editor">
        <div className="editor__image">
          <canvas id="canvas" data-image></canvas>
        </div>
        <div className="editor__controls">
          <div className="section">
            <h2>Select adjustment mode</h2>
            <div className="buttons">
                <button data-mode="normal">Normal mode</button>
                <button data-mode="colors">Only colors mode</button>
            </div>
          </div>

          <div className="section">
            <h2>Adjust image</h2>
            <AppProvider>
              <Slider label="Positive" namespace="slider1" />
              <Slider label="Negative" namespace="slider2" />
            </AppProvider>
            <h3>Presets</h3>
            <div className="buttons">
              <button data-preset="original" data-pos="0" data-neg="0">Show Original</button>
              <button data-preset="suggestion" data-pos="1" data-neg="1">Show Suggestion</button>
            </div>
          </div>
        </div>
      </div>
  );
}

export default App;
