import React, { useState, useContext } from 'react';
import uniqueId from 'lodash.uniqueid';
import { AppContext } from '../../provider';

const Slider = ({ label, min, max, namespace}) => {

    const useSlider = () => {
        const contextValue = useContext(AppContext);
        return contextValue;
      };

    const [id] = useState(() => uniqueId('slider-'));
    const [slider, dispatch] = useSlider();

    return (
        <div className="slider">
            <label className="slider__label" htmlFor={ id }>{ label }</label>
            <input 
                type="range" 
                onChange={ (e) => dispatch({type: "UPDATE_SLIDER", payload: e.target.value, namespace: namespace}) } 
                id={ id } 
                min={ min } 
                max={ max } 
                value={ slider[namespace] } 
                className="range slider__range" 
            />

            <div className="indicator slider__indicator">
                <span className="indicator__value">0.0</span>
                <span className="indicator__value">0.5</span>
                <span className="indicator__value">1.0</span>
                <span className="indicator__value">1.5</span>
                <span className="indicator__value">2.0</span>
            </div>
        </div>
    );
}

export default Slider;
